// interface for geometric shapes of all kinds

public interface GeometricShape<T extends GeometricShape> {
    public void describe();
    public T supersized();
}
