
class GoodArrayExample2 {
    public static void main(String[] args) {
        try {
            GeometricShape[] geoshapes;
            geoshapes = new Circle[2];

            geoshapes[0] = new Circle(1.0);
            geoshapes[1] = new Cone(2.0, 3.0);
        }
        catch(ArrayStoreException err) {
            System.out.println("Exception was caught: " + err);
        }
    }
}
