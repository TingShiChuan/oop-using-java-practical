

class ArrayExample {
    public static void main(String[] args) {

        GeometricShape[] geoshapes;
        geoshapes = new Circle[2];

        geoshapes[0] = new Circle(1.0);
        geoshapes[1] = new Cone(2.0, 3.0);

    }
}