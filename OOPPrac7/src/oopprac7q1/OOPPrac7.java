/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac7q1;

/**
 *
 * @author ting
 */
public class OOPPrac7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //New Thread
        
        PrintThread pt = new PrintThread("Printer1");
        PrintThread pt1 = new PrintThread("Printer2");
        PrintThread pt2 = new PrintThread("Printer3");
        PrintThread pt3 = new PrintThread("Printer4");
        
        
        
        pt.start();
        pt1.start();
        pt2.start();
        pt3.start();
        
        PrintThread2 p1 = new PrintThread2("Printer1");
        PrintThread2 p2 = new PrintThread2("Printer2");
        PrintThread2 p3 = new PrintThread2("Printer3");
        PrintThread2 p4 = new PrintThread2("Printer4");
        
        new Thread(p1).start();
        new Thread(p2).start();
        new Thread(p3).start();
        new Thread(p4).start();
    }
    
}
