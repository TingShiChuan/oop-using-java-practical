/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac7q1;

/**
 *
 * @author ting
 */
public class PrintThread extends Thread {
    
    
    public PrintThread(String name) {
        super(name);
    }
    
    public void run() {
        try {
   // print thread name going to sleep
            System.out.println(Thread.currentThread().getName() + " is going to sleep");
            Thread.sleep(1000);
            
   // put the given thread to sleep for a random interval sleeptime
        } 
        catch (InterruptedException exception ) {
             System.err.println( exception.toString() );
        }
        
        
        // print thread name done sleeping
        System.out.println(Thread.currentThread().getName() + " is awaked!");

    }
}
