/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac7q2;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ting
 */
public class Odd extends Thread{
    
    public void run() {
        for(int i = 1; i < 11; i+=2) {
            try {
                System.out.println("Output from Odd ====> " + i);
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Odd.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }
        
    }
}
