/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package revisep3;

import java.util.ArrayList;

/**
 *
 * @author ting
 */
public class ReviseP3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {

            MyGeneric<String> i = new MyGeneric();
            
            String[] test1 = new String[4];
            test1[0] = "a";
            test1[1] = "b";
            test1[2] = "c";
            test1[3] = "d";
            i.exchange(test1, 1, 2);

            MyGeneric<Integer> j = new MyGeneric();
            
            Integer[] test2 = new Integer[4];
            test2[0] = 1;
            test2[1] = 2;
            test2[2] = 3;
            test2[3] = 4;

            j.exchange(test2, 1, 5);
            
            MyGeneric<String> [] my = new MyGeneric[10];
        }
        catch(ArrayIndexOutOfBoundsException ex) {
            System.out.println(ex);
        }
    }
    
    public static <T extends Number> T max(T x, T y) {
        return x > y ? x : y; 
    }
    
}
