/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author ting
 */
public class OOPPrac2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create file to write first message

        Scanner s = new Scanner(System.in);
        try {

            File first = new File("firstversion.txt");

            if (!first.exists()) {
                if (first.createNewFile()) {
                    System.out.println("File created!");
                }
            }

            PrintWriter input = new PrintWriter(first);

            System.out.printf("Write your secret message: ");

            String message = s.nextLine();
            input.printf(message);

            input.close();

            encryptFile(first);

            //Read the second file
            File second = new File("secondversion.txt");
            if (!second.exists()) {
                System.out.print("Error");
            }

            Scanner s2 = new Scanner(second);
            String output;
            while (s2.hasNextLine()) {
                System.out.print(s2.nextLine());
            }
            System.out.println("\n");

            //Test for decryption
            decryptFile(second);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static void encryptFile(File first) {

        try {
            Scanner s = new Scanner(first);
            File second = new File("secondversion.txt");

            if (second.exists()) {
                second.delete();
            }

            if (second.createNewFile()) {
                System.out.println("File created!");
            }

            FileWriter input = new FileWriter(second, true);
            BufferedWriter bW = new BufferedWriter(input);

            String data;
            //encrypt each characters in the file by adding 10 ASCII code value
            while (s.hasNextLine()) {
                data = s.nextLine();
                char c;
                for(char a : data.toCharArray()) {
                    if(a != ' ') {
                        a += 10;
                        bW.write(Character.toChars(a));
                    }
                    else {
                        bW.write(" ");
                    }
                }/*
                for (int i = 0; i < data.length(); i++) {
                    if (data.charAt(i) != ' ') {
                        int value = data.charAt(i);
                        value += 10;
                        bW.write(Character.toChars(value));
                    } else {
                        bW.write(" ");
                    }
                }*/

            }

            bW.close();

            input.close();

            System.out.println("Content Encrypted!");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static void decryptFile(File file) {

        try {
            Scanner s = new Scanner(file);
            String data;
            while (s.hasNextLine()) {
                data = s.nextLine();
                char c;
                for (int i = 0; i < data.length(); i++) {
                    if (data.charAt(i) != ' ') {
                        int value = data.charAt(i);
                        value -= 10;
                        System.out.print(Character.toChars(value));
                    }
                    else {
                        System.out.print(" ");
                    }
                }

            }

            System.out.println("\nDecrypted!\n");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
