/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab10;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ting
 */
public class Server implements Runnable {
    
    private Socket clientSoc;
    
    Server(Socket clientSoc) {
        this.clientSoc = clientSoc;
    }
    
    public static void main(String [] args) throws IOException {
        try {
            ServerSocket ss = new ServerSocket(1234);   
            while(true) {
                Socket client = ss.accept();
                System.out.println("Connection Successful!");
                new Thread(new Server(client)).start();
                
            }
            
        }
        catch(IOException err) {
            err.printStackTrace();
        }
        
    }
    
    public void run() {
        
        try {
            //PrintStream out = new PrintStream(clientSoc.getOutputStream());
            
            //BufferedReader in = new BufferedReader(new InputStreamReader(clientSoc.getInputStream()));
            DataInputStream is = new DataInputStream(clientSoc.getInputStream());
            DataOutputStream os = new DataOutputStream(clientSoc.getOutputStream());
            ObjectOutputStream oos = new ObjectOutputStream(clientSoc.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(clientSoc.getInputStream());
            while(true) {
                
                
                String line = is.readUTF();
                System.out.println(line);
                os.writeUTF("ECHO FROM SERVER: " + line);
                
                List<String> i = new ArrayList();
                oos.writeObject(i);
                
                List<String> a = (List)ois.readObject();
                
                if(line.equals("exit")) {
                    System.out.println("exit!!");
                    break;
                }
                
            }
            //out.close();    
            //in.close();
            System.out.println("Connection closed");
            os.close();
            is.close();
            clientSoc.close();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
