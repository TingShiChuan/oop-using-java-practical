/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab10;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author ting
 */
public class Lab10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Socket client;
        Scanner s = new Scanner(System.in);
        String input = "";
        try {
            client = new Socket("IchikawaPro.local", 1234);
            //PrintStream out = new PrintStream(client.getOutputStream());
            //BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            DataInputStream is = new DataInputStream(client.getInputStream());
            DataOutputStream os = new DataOutputStream(client.getOutputStream());

            while(true)
            {
                
                System.out.printf("Enter your message: ");
                input = s.nextLine();
                //out.println(input);
                os.writeUTF(input);
                if(input.equals("exit"))
                {
                    break;
                }
                String line = is.readUTF();
                System.out.println(line);

                //printwritter -> send to server
                //bufferreader -> getfrom server
            }
            //out.close();
            //in.close();
            os.close();
            is.close();
            client.close();
        }
        catch(IOException err) {
            err.printStackTrace();
        }
    }
    
}
