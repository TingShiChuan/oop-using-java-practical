/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab10;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ting
 */
public class ServerQ2 {
 
     public static void main(String [] args) throws IOException {
        try {
            ServerSocket ss = new ServerSocket(1234);   
            
            Socket client = ss.accept();
            
            ObjectInputStream is = new ObjectInputStream(client.getInputStream());
            ObjectOutputStream os = new ObjectOutputStream(client.getOutputStream());
                
            Loan l = (Loan)is.readObject();
            double result = (l.loanAmount*(Math.pow(1 + l.rate, l.month) * l.rate)) / (Math.pow(1 + l.rate, l.month) - 1);
            System.out.println(result);
            l.setMonthlyPayment(result);
            
            os.writeObject(l);
            
            
        }
        catch(IOException err) {
            err.printStackTrace();
        } catch (ClassNotFoundException ex) {
             Logger.getLogger(ServerQ2.class.getName()).log(Level.SEVERE, null, ex);
         }
        
    }
}
