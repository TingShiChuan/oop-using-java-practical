/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab10;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ting
 */
public class Lab10Q2 {
    public static void main(String[] args) {
        
        Socket client;
        Scanner s = new Scanner(System.in);
        String input = "";
        try {
            client = new Socket("localhost", 1234);
            ObjectOutputStream os;
            ObjectInputStream is;
            os = new ObjectOutputStream(client.getOutputStream());
            is = new ObjectInputStream(client.getInputStream());
            
            System.out.printf("Enter your <annual interest rate> <no of months> <loan amount");
            Loan newLoan = new Loan(s.nextInt(), s.nextInt(), s.nextDouble());
            os.writeObject(newLoan);
            
            Loan l = (Loan)is.readObject();
            System.out.println("Monthly Payment is : "+l.getMonthlyPayment());
            is.close();
            os.close();
            client.close();
        }
        catch(IOException err) {
            err.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Lab10Q2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
