/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab10;

import java.io.*;

/**
 *
 * @author ting
 */
public class Loan implements Serializable {
    
    public double loanAmount;
    public int rate;
    public int month;
    private double monthlyPayment;
    Loan(int r, int n, double p) {
        this.loanAmount = p;
        this.rate = r;
        this.month = n;
    }
    
    public void setMonthlyPayment(double M) {
        this.monthlyPayment = M;
    }
    
    public double getMonthlyPayment() {
        return this.monthlyPayment;
    }
}
