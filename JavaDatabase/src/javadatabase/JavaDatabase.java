/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.*;

/**
 *
 * @author ting
 */
public class JavaDatabase {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // TODO code application logic here
        
        Class.forName("com.mysql.jdbc.Driver");
        
        
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/ebookshop", "root", "");
        
        //Check is table created
        DatabaseMetaData metadata = connection.getMetaData();
        ResultSet resultSet;
        resultSet = metadata.getTables(null, null, "BOOKS", null);

        Statement statement = connection.createStatement();
        
        if(!(resultSet.next())) {
            
        
            int result = statement.executeUpdate(
                "CREATE TABLE BOOKS " +
                            "(id INTEGER not NULL, " +
                            " title VARCHAR(255), " +
                            " author VARCHAR(255), " +
                            " price float, " +
                            " qty INTEGER, "+
                            " PRIMARY KEY(id))");
            System.out.println(result);
            System.out.println("Table is created!");
        }
        else {
            System.out.println("Table is created!");
        }
        
        int result = statement.executeUpdate(
                "INSERT INTO BOOKS VALUES "
                        + "(1001, 'Java for dummies', 'Tan Ah Teck', 11.11, 11),"
                        + "(1002, 'More Java for dummies', 'Tan Ah Teck', 22.22, 22),"
                        + "(1003, 'More Java for more dummies', 'Mohammad Ali', 33.33, 33),"
                        + "(1004, 'A Cup of Java', 'Kumar', 44.44, 44),"
                        + "(1005, 'A Teaspoon of Java', 'Kevin Jones', 55.55, 55)");
        
        
        if(result == 0) {
            System.out.println("Insert value successful!");
        }
        
        resultSet = statement.executeQuery(
                    "SELECT * FROM BOOKS");
        
        while(resultSet.next()) {
            System.out.println(
                resultSet.getString(1) + ", " +
                resultSet.getString(2) + ", " +
                resultSet.getString(3) + " " 
            );
        }
        
    }
    
}
