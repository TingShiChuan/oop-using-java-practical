/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.*;

/**
 *
 * @author ting
 */
public class Q2 {
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("===Question 2===");
        System.out.println("Enter an integer list: (Exit with 0)");
        LinkedList<Integer> q2 = new LinkedList();
        ArrayList<Integer> uniqueV = new ArrayList();
        
        Scanner sc = new Scanner(System.in);
        int input;
        do {
            input = sc.nextInt();
            if(input != 0) {
                q2.add(input);
            }
        }
        while(input != 0);
        
        //copyOfQ2 = (LinkedList)q2.clone();
        
        int [] counts = new int[q2.size()];
        
        int index = 0;
        int max = 0;
        //Loop if link list is not empty
        while(!q2.isEmpty()) {
            //get the first value of the link list
            int cur = q2.get(0);
            uniqueV.add(cur);
            while(q2.contains(Integer.valueOf(cur))) {
                counts[index]++;    //add 1 to count if there is any occurance
                q2.remove(Integer.valueOf(cur)); //add remove the occurance to prevent duplication count
                
            }
            
            //Check for max count
            if(max < counts[index]) {
                max = counts[index];
            }
            index++;
        }
        
        System.out.println("Value that has highest number of occurance: ");
        for(int j = 0; j < counts.length; j++) {
            if(counts[j] == max) {
                System.out.print(uniqueV.get(j) + ", ");
            }
        }
        System.out.println();
        
    }
    
}
