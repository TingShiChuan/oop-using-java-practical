/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ting
 */
public class Q3 {
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("===Question 3===");
        System.out.println("Enter a string lst: (Exit with 0)");
        ArrayList<String> q3 = new ArrayList();
        
        
        Scanner sc = new Scanner(System.in);
        String input;
        do {
            input = sc.next();
            if(!input.equals("0")) {
                q3.add(input);
            }
        }
        while(!(input.equals("0")));
        
        
        System.out.println("\n===BEFORE===");
        for(String data : q3)
            System.out.println(data);
        switchPairs(q3);
        System.out.println("\n===AFTER===");
        for(String data : q3)
            System.out.println(data);
    }
    
    private static void switchPairs(ArrayList<String> array) {
        
        String temp;
        for(int i = 0; i < array.size(); i+=2) {
            if(i != array.size()-1){

                temp = array.get(i);
                array.set(i, array.get(i+1));
                array.set(i+1, temp);
            }
        }

        
    }
}
