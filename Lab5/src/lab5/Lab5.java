/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.*;

/**
 *
 * @author ting
 */
public class Lab5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("===Question 1===");
        LinkedList<Integer> q1 = new LinkedList();
        q1.add(1);
        q1.add(4);
        q1.add(2);
        q1.add(5);
        
        //check number 4
        System.out.println("Check number 6 in linked list: " + q1.contains(4));
        
        //check number 6
        System.out.println("Check number 6 in linked list: " + q1.contains(6));
        
        System.out.println("===Question 2===");
        LinkedList<Integer> q2 = new LinkedList();
        q2.add(1);
        q2.add(3);
        q2.add(4);
        q2.add(7);
        q2.add(3);
        q2.add(7);
        q2.add(2);
        
        q2.remove(Integer.valueOf(7));
        
        for(Integer i : q2) {
            System.out.println(i);
        }
         
        System.out.println("===Question 3===");
        
        LinkedList<Integer> q3 = new LinkedList();
        q3.add(1);
        q3.add(3);
        q3.add(4);
        q3.add(7);
        q3.add(3);
        q3.add(7);
        q3.add(2);
        
       
        
        while(q3.contains(7))
            q3.remove(Integer.valueOf(7));
        
        for(Integer i : q3) {
            System.out.println(i);
        }
         
        System.out.println("===Question 4===");
        
        LinkedList<Integer> q4 = new LinkedList();
        q4.add(1);
        q4.add(3);
        q4.add(4);
        q4.add(7);
        q4.add(3);
        q4.add(7);
        q4.add(2);
        
        System.out.println("Counting for occurence of number 7");
        int i = 0;
        for(Integer data : q4) {
            if(data == 7) {
                i++;
            }
        }
        System.out.println("Counting for occurence of number 7: " + i);
        
        System.out.println("===Question 5===");
        LinkedList<Integer> q5 = new LinkedList();
        q5.add(2);
        q5.add(4);
        q5.add(5);
        q5.add(8);
        q5.add(1);
        q5.add(5);
        q5.add(3);
        
        //check index of number 5
        System.out.println("Check index of number 5 in linked list: " + q5.indexOf(5));
    }
    
}
