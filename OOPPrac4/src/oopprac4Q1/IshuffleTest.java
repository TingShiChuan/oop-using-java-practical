/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac4Q1;

import java.util.ArrayList;

/**
 *
 * @author ting
 */
public class IshuffleTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> list1 = new ArrayList();
        ArrayList<Integer> list2 = new ArrayList();
        
        list1.add("One");
        list1.add("two");
        list1.add("three");
        list1.add("four");
        
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        
        
        Ishuffle.shuffle(list1);
        Ishuffle.shuffle(list2);
        
        for(String item: list1) {
            System.out.println(item);
        }
        
        for(Integer item: list2) {
            System.out.println(item.toString());
        }
    }
    
}
