/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac4Q1;

import java.util.*;

/**
 *
 * @author ting
 */
public class Ishuffle {
    
    Ishuffle() {
        
    }
    
    public static <E> void shuffle (ArrayList<E> list) {
        long seed = System.nanoTime();
        Collections.shuffle(list, new Random(seed));
    }
}
