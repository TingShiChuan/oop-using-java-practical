/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac4Q2;

import oopprac4Q1.*;
import java.util.ArrayList;

/**
 *
 * @author ting
 */
public class IshuffleTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> list1 = new ArrayList();
        ArrayList<Integer> list2 = new ArrayList();
        
        list1.add("One");
        list1.add("two");
        list1.add("three");
        list1.add("four");
        
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        
        Ishuffle<String> listShuffle1 = new Ishuffle(list1);
        Ishuffle<Integer> listShuffle2 = new Ishuffle(list2);
        
        listShuffle1.shuffle();
        listShuffle2.shuffle();
        
        for(String item: listShuffle1.get()) {
            System.out.println(item);
        }
        
        for(Integer item: listShuffle2.get()) {
            System.out.println(item.toString());
        }
        
        
    }
    
}
