/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac4Q2;

import oopprac4Q1.*;
import java.util.*;

/**
 *
 * @author ting
 */
public class Ishuffle<E> {
    
    private ArrayList<E> list;
    
    Ishuffle() {
        
    }
    
    Ishuffle(ArrayList<E> list) {
        this.list = list;
    }
    
    public void set(ArrayList<E> list) {
        this.list = list;
    }
    
    
    public ArrayList<E> get() {
        return list;
    }
    
    public void shuffle() {
        long seed = System.nanoTime();
        Collections.shuffle(list, new Random(seed));
    }

}
