/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcquiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ting
 */
public class McQuiz {

// Driver method to test below methods
    public static void main(String[] args) {
        System.out.println("First Test");
        String input = "AMHSIW";
        char set1[] = {'A', 'M', 'H', 'S', 'I', 'W'};
        permute(input);

    }

    static void permute(String input) {
        int inputLength = input.length();
        boolean[] used = new boolean[inputLength];
        StringBuffer outputString = new StringBuffer();
        char[] in = input.toCharArray();

        doPermute(in, outputString, used, inputLength, 0);

    }

    static void doPermute(char[] in, StringBuffer outputString,
            boolean[] used, int inputLength, int level) {
        if (level == inputLength) {
            //System.out.println(outputString.toString());
            checkDictionary(outputString.toString());
            return;
        }

        for (int i = 0; i < inputLength; ++i) {

            if (used[i]) {
                continue;
            }

            outputString.append(in[i]);
            used[i] = true;
            doPermute(in, outputString, used, inputLength, level + 1);
            used[i] = false;
            outputString.setLength(outputString.length() - 1);
        }
    }
}
