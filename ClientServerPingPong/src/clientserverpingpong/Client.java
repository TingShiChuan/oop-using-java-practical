/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserverpingpong;

import java.io.*;
import java.net.*;

/**
 *
 * @author ting
 */
public class Client {
    public static void main(String[] args) {
        try {
            Socket client = new Socket("localhost", 1026);
            
            DataOutputStream os = new DataOutputStream(client.getOutputStream());
            
            DataInputStream is = new DataInputStream(client.getInputStream());
            os.writeUTF("Ping");
            
            String line = is.readUTF();
            System.out.println(line);
            
            os.close();
            is.close();
            client.close();
        }
        catch(IOException err) {
            err.printStackTrace();
        }
    }
    
}
