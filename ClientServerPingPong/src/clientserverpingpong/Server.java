/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserverpingpong;

import java.io.*;
import java.net.*;

/**
 *
 * @author ting
 */
public class Server {
    public static void main (String [] args) throws IOException
    {
        try {
            ServerSocket s = new ServerSocket(1026);
            Socket client = s.accept();
            System.out.println("Connection Successful!");
            DataInputStream is = new DataInputStream(client.getInputStream());
            DataOutputStream os = new DataOutputStream(client.getOutputStream());
            String line = is.readUTF();
            System.out.println(line);
            if(line.equals("Ping"))
            {
                os.writeUTF("Pong");
            }
            
            
            os.close();
            is.close();
            client.close();
            s.close();
        }
        catch(IOException err) {
            err.printStackTrace();
        }
        
    }
    
}
