/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Student;

import chatroom.ClientGUI;
import chatroom.ServerGUI;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JComponent;
import javax.swing.JPanel;
import superclass.Client;
import superclass.QuizQuestion;
import superclass.SuperClass;

/**
 *
 * @author janicechau
 */
public class EachQuestionResult extends javax.swing.JFrame {

    /**
     * Creates new form EachQuestionResult
     */
    private int choice1;
    private int choice2;
    private int choice3;
    private int choice4;

    TimerTask tt;
    private SuperClass sc;
    private Client client;
    private int qCount;

    private QuizQuestion qq;

    /**
     * Creates new form CorrectWrong
     */
    public EachQuestionResult(QuizQuestion qq, Client client, SuperClass sc, int qCount) {
        this.qq = qq;
        this.sc = sc;
        this.client = client;
        this.qCount = qCount;

        this.choice1 = sc.getQuesByIndex(qCount).getChoiceByIndex(0).getTotalSelected();
        this.choice2 = sc.getQuesByIndex(qCount).getChoiceByIndex(1).getTotalSelected();
        this.choice3 = sc.getQuesByIndex(qCount).getChoiceByIndex(2).getTotalSelected();
        this.choice4 = sc.getQuesByIndex(qCount).getChoiceByIndex(3).getTotalSelected();

        initComponents();
        for (Client c : qq.getRanking()) {
            Ranking1.add(c.getName(), 0);
            Ranking1.add(c.toString(c.getScore()), 1);
        }

        this.isAlwaysOnTop();
        Timer time = new Timer();
        System.out.println("Created Timer");
        tt = new showResultTimer(this, this.sc.getQuesByIndex(qCount), client, sc, qCount);
        time.schedule(tt,
                0, 1 * 1000);
    }

    class showResultTimer extends TimerTask {

        private SuperClass sc;
        private Client client;
        private int qCount;
        private int secondLeft;
        private EachQuestionResult result;
        private QuizQuestion qq;

        showResultTimer(EachQuestionResult result, QuizQuestion qq, Client client, SuperClass sc, int qCount) {
            this.result = result;
            this.qq = qq;
            this.client = client;
            this.sc = sc;
            this.qCount = qCount;
            secondLeft = 0;

        }

        @Override
        public void run() {

            if (secondLeft < 5) {
                //System.out.println("CountDown :"+ secondLeft-- + "second");
                secondLeft++;
            } else {
                qCount++;
                result.setVisible(false);
                if (qCount >= this.sc.getAllQuestions().size()) {
                    this.cancel();
                    ClientGUI.main(new String[]{"Client Side"});
                } else {
                    new ShowQuestion(client, sc, qCount).setVisible(true);
                }
                //new ShowQuestion(sc, qCount);
                //System.exit(0);
            }
        }

    }

    public EachQuestionResult() {
        initComponents();

    }

    public EachQuestionResult(int choice1, int choice2, int choice3, int choice4) {
        this.choice1 = choice1;
        this.choice2 = choice2;
        this.choice3 = choice3;
        this.choice4 = choice4;

        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        showRectFrame = new javax.swing.JPanel();
        getContentPane().setBackground(new java.awt.Color(52, 73, 94));
        showRectFrame1 = new javax.swing.JPanel();
        showRectFrame2 = new javax.swing.JPanel();
        showRectFrame3 = new javax.swing.JPanel();
        lblChoice1 = new javax.swing.JLabel();
        lblChoice4 = new javax.swing.JLabel();
        lblChoice2 = new javax.swing.JLabel();
        lblChoice3 = new javax.swing.JLabel();
        Ranking1 = new java.awt.List();
        label1 = new java.awt.Label();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(52, 73, 94));

        showRectFrame.setBackground(new java.awt.Color(46, 204, 113));

        javax.swing.GroupLayout showRectFrameLayout = new javax.swing.GroupLayout(showRectFrame);
        showRectFrame.setLayout(showRectFrameLayout);
        showRectFrameLayout.setHorizontalGroup(
            showRectFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 52, Short.MAX_VALUE)
        );
        showRectFrameLayout.setVerticalGroup(
            showRectFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );

        showRectFrame1.setBackground(new java.awt.Color(241, 196, 15));
        showRectFrame1.setForeground(new java.awt.Color(255, 255, 255));
        showRectFrame1.setToolTipText("");

        javax.swing.GroupLayout showRectFrame1Layout = new javax.swing.GroupLayout(showRectFrame1);
        showRectFrame1.setLayout(showRectFrame1Layout);
        showRectFrame1Layout.setHorizontalGroup(
            showRectFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 52, Short.MAX_VALUE)
        );
        showRectFrame1Layout.setVerticalGroup(
            showRectFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        showRectFrame2.setBackground(new java.awt.Color(230, 126, 34));

        javax.swing.GroupLayout showRectFrame2Layout = new javax.swing.GroupLayout(showRectFrame2);
        showRectFrame2.setLayout(showRectFrame2Layout);
        showRectFrame2Layout.setHorizontalGroup(
            showRectFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 52, Short.MAX_VALUE)
        );
        showRectFrame2Layout.setVerticalGroup(
            showRectFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 105, Short.MAX_VALUE)
        );

        showRectFrame3.setBackground(new java.awt.Color(52, 152, 219));

        javax.swing.GroupLayout showRectFrame3Layout = new javax.swing.GroupLayout(showRectFrame3);
        showRectFrame3.setLayout(showRectFrame3Layout);
        showRectFrame3Layout.setHorizontalGroup(
            showRectFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 52, Short.MAX_VALUE)
        );
        showRectFrame3Layout.setVerticalGroup(
            showRectFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 11, Short.MAX_VALUE)
        );

        lblChoice1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        lblChoice1.setForeground(new java.awt.Color(255, 255, 255));
        lblChoice1.setText("2");

        lblChoice4.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        lblChoice4.setForeground(new java.awt.Color(255, 255, 255));
        lblChoice4.setText("0");

        lblChoice2.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        lblChoice2.setForeground(new java.awt.Color(255, 255, 255));
        lblChoice2.setText("2");

        lblChoice3.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        lblChoice3.setForeground(new java.awt.Color(255, 255, 255));
        lblChoice3.setText("3");

        Ranking1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ranking1ActionPerformed(evt);
            }
        });

        label1.setFont(new java.awt.Font("Comic Sans MS", 1, 48)); // NOI18N
        label1.setSize(new java.awt.Dimension(100, 80));
        label1.setText("Result for the Question");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(366, 366, 366)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblChoice1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(showRectFrame1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(showRectFrame2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(showRectFrame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblChoice2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblChoice3, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(showRectFrame3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblChoice4, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(131, 131, 131)
                .addComponent(Ranking1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(389, 389, 389))
            .addGroup(layout.createSequentialGroup()
                .addGap(349, 349, 349)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Ranking1, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(showRectFrame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(showRectFrame1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(showRectFrame2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(showRectFrame3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblChoice1)
                            .addComponent(lblChoice4)
                            .addComponent(lblChoice2)
                            .addComponent(lblChoice3))))
                .addGap(170, 170, 170))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Ranking1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ranking1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Ranking1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EachQuestionResult.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EachQuestionResult.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EachQuestionResult.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EachQuestionResult.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EachQuestionResult().setVisible(true);
            }
        });
    }

    class MyRect extends JPanel {

        public void paint(Graphics g) {
            g.drawRect(10, 10, 200, 200);
            g.setColor(Color.red);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.List Ranking1;
    private javax.swing.JFrame jFrame1;
    private java.awt.Label label1;
    private javax.swing.JLabel lblChoice1;
    private javax.swing.JLabel lblChoice2;
    private javax.swing.JLabel lblChoice3;
    private javax.swing.JLabel lblChoice4;
    private javax.swing.JPanel showRectFrame;
    private javax.swing.JPanel showRectFrame1;
    private javax.swing.JPanel showRectFrame2;
    private javax.swing.JPanel showRectFrame3;
    // End of variables declaration//GEN-END:variables
}
