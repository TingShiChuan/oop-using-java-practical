/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

import java.io.*;
import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.jdbc.ScriptRunner;
import superclass.QuizQuestion;
import superclass.SuperClass;

/**
 *
 * @author ting
 */
public class Database {

    private String dbUrl;
    private String usrName;
    private String pW;
    private String schemaName;
    private static String driverUrl = "com.mysql.jdbc.Driver";
    private static String[] scripts = {
        "SuperClass_classroom.sql",
        "SuperClass_Client.sql",
        "SuperClass_quizanswer.sql",
        "SuperClass_quizques.sql",
        "SuperClass_student.sql"
    };

    public static String[] tables = {
        "classroom",
        "Client",
        "quizanswer",
        "quizques",
        "student"
    };

    Database(String dbUrl, String schemaName, String usrName, String pW) {
        this.dbUrl = dbUrl;
        this.schemaName = schemaName;
        this.usrName = usrName;
        this.pW = pW;
    }

    public String getSchema() {
        return this.schemaName;
    }

    public String getDbUrl() {
        return this.dbUrl;
    }

    public void initDB() throws SQLException, ClassNotFoundException {
        //Load driver
        Class.forName(driverUrl);
        for (String script : scripts) {
            try {
                new ScriptRunner(
                        DriverManager.getConnection(dbUrl, usrName, pW))
                        .runScript(new BufferedReader(new FileReader("mysqlscripts/" + script)));
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    public void insertDB(String query) {
        Connection connection;
        try {
            //Load Driver
            Class.forName(driverUrl);
            //Create db connection
            connection = DriverManager.getConnection(dbUrl + this.schemaName, usrName, pW);
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate(query);
            if (result == 0) {
                System.out.println("Record inserted!");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet retriveDB(String query) {
        Connection connection;
        try {
            //Load Driver
            Class.forName(driverUrl);
            //Create db connection
            connection = DriverManager.getConnection(dbUrl + this.schemaName, usrName, pW);
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Load Demo Data from Database
    public void loadSuperClassData(String classCode) {

        //Declare variables
        Connection connection;
        ResultSet demoSet;
        ResultSet answerSet;
        SuperClass demo = null;

        boolean classLoaded = false;

        String query;
        try {
            Class.forName(driverUrl);
            connection = DriverManager.getConnection(dbUrl + this.schemaName, usrName, pW);
            Statement sta = connection.createStatement();

            //Create a demo classroom
            demo = new SuperClass(classCode);

            //Get all demo questions
            query = "SELECT qq.* FROM classroom cr "
                    + "JOIN quizques qq "
                    + "ON qq.classroomId = cr.crId "
                    + "JOIN quizanswer qa "
                    + "ON qa.quizquesId = qq.qqId "
                    + "WHERE cr.crId = '" + classCode + "' "
                    + "GROUP BY qq.question";

            demoSet = sta.executeQuery(query);

            while (demoSet.next()) {

                QuizQuestion qq = new QuizQuestion(
                        demoSet.getString(1),
                        demoSet.getString(2),
                        Integer.parseInt(demoSet.getString(5)),
                        Integer.parseInt(demoSet.getString(3)));
                //Add question to demo class
                demo.addQuestion(qq);
            }

            //Get all answer choices of each question
            query = "SELECT qa.* FROM classroom cr "
                    + "JOIN quizques qq "
                    + "ON qq.classroomId = cr.crId "
                    + "JOIN quizanswer qa "
                    + "ON qa.quizquesId = qq.qqId "
                    + "WHERE cr.crId = '" + classCode + "' "
                    + "order by qa.quizansId";

            answerSet = sta.executeQuery(query);
            int i = 0;
            int curIndex = 0;
            int index = 0;
            while (answerSet.next()) {
                //Get the index of the current question id
                curIndex = demo.getQuesIndexById(answerSet.getString(2));
                if (curIndex != index) {     //Check if different question
                    i = 0;                  //reset choice index to 0;
                    index = curIndex;
                }

                if (answerSet.getString(4).equals("1")) {
                    System.out.println(i);
                    demo.getQuesByIndex(curIndex).setChoices(i, answerSet.getString(3), true);

                } else {
                    demo.getQuesByIndex(index).setChoices(i, answerSet.getString(3), false);
                }
                i++;
                classLoaded = true;
                System.out.println("213213123123");
            }
            if (classLoaded) {
                MultiThreadedServer.classrooms.add(demo);
            }
            //System.out.println(demo.getClassId());

        } catch (SQLException | ArrayIndexOutOfBoundsException err) {
            err.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        //printAllQuestion(demo);
    }

    public void printAllQuestion(SuperClass demo) {
        List<QuizQuestion> questions = demo.getAllQuestions();
        int i = 1;
        for (QuizQuestion q : questions) {
            System.out.print(i);
            System.out.print("\t" + q.getQuestion());
            System.out.print("\t" + q.getScore());
            System.out.print("\t" + q.getTiming());
            System.out.print("\n");
            for (int j = 0; j < q.getAllChoices().length; j++) {
                if (q.getChoiceByIndex(j) != null) {
                    System.out.print(j);
                    System.out.print("\t" + q.getChoiceByIndex(j).getDescription());
                    System.out.print("\t" + q.getChoiceByIndex(j).getIsCorrect());
                    System.out.print("\n");
                }
            }
            System.out.print("\n\n");
        }
    }

    public Boolean checkForSchema() {
        try {
            //Check is schema created
            Class.forName(driverUrl);
            //Create db connection
            Connection connection = DriverManager.getConnection(dbUrl, usrName, pW);
            DatabaseMetaData metadata = connection.getMetaData();
            System.out.print(schemaName);
            ResultSet rs = metadata.getTables(schemaName, null, "Client", null);
            if (!(rs.next())) {
                return false;
            } else {
                return true;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ResultSet getClassroom(String classCode) {
        try {
            //Check is schema created
            Class.forName(driverUrl);
            //Create db connection
            Connection connection = DriverManager.getConnection(dbUrl + schemaName, usrName, pW);
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(
                    "SELECT * from " + Database.tables[0]
                    + " WHERE crId='" + classCode + "'"
            );
            if (!(rs.next())) {
                return null;
            } else {
                return rs;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
