/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

import java.io.*;
import java.net.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import superclass.*;

/**
 *
 * @author ting
 */
public class MultiThreadedServer implements Runnable {

    private Client client;

    public static List<Client> clients = new ArrayList<>();
    public static List<SuperClass> classrooms = new ArrayList<>();
    //Boolean go = false;
    private SuperClass curClass;
    public static Database db;
    private static Thread tempT;

    private Thread curThread;

    MultiThreadedServer(Client client) {
        this.client = client;
    }

    public Thread.State getState() {
        return curThread.getState();
    }

    public static void main(String[] args) throws IOException {
        try {
            ServerSocket ss = new ServerSocket(1234);

            //Initialize MYSQL database
            try {
                db = new Database(
                        "jdbc:mysql://localhost/",
                        "SuperClass",
                        "root", ""
                );
                if (!(db.checkForSchema())) //Check if schema exist in db
                {
                    db.initDB();
                }
            } catch (SQLException | ClassNotFoundException err) {
                System.out.println(err);
            }

            while (true) {
                Socket client = ss.accept();
                try {
                    Client newClient;
                    System.out.println("Connection Successful!");
                    ObjectInputStream is = new ObjectInputStream(client.getInputStream());

                    //Create a new client object
                    newClient = (Client) is.readObject();
                    newClient.setSocket(client);

                    //check if client already logon
                    Client checkC = findClientById(newClient.getClientId(), newClient.getType());
                    if (checkC == null) {
                        newClient.setClientId(newClient.getSocket().getInetAddress().toString().split("/")[1]);

                        //Check if client already exist
                        String retriveQuery = "SELECT cId from "
                                + Database.tables[1]
                                + " where cId='" + newClient.getClientId() + "'";

                        ResultSet result = db.retriveDB(retriveQuery);

                        if (result.next()) {
                            //Update Last Login Time
                            String query = "UPDATE " + Database.tables[1]
                                    + " SET lastlogin='"
                                    + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                                    + "' where cId='" + newClient.getClientId() + "'";
                            db.insertDB(query);
                            System.out.println("Returning Client Detected!");
                        } else {
                            //Insert new client record
                            String query = "INSERT INTO " + Database.tables[1] + " VALUES ('"
                                    + newClient.getClientId() + "', '"
                                    + newClient.getName() + "', '"
                                    + newClient.getType() + "', '"
                                    + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())) + "', '"
                                    + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())) + "')";

                            System.out.println(query);
                            db.insertDB(query);
                            System.out.println("Inserted new Client!");
                        }
                        clients.add(newClient);
                    } else {

                        //remove same client
                        clients.remove(checkC);
                        System.out.println("Killed exisiting User!");
                        System.out.println(newClient.isReady());
                        clients.add(newClient);
                    }
                    tempT = new Thread(new MultiThreadedServer(newClient));
                    tempT.start();

                } catch (ClassNotFoundException err) {
                    Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, err);
                } catch (IOException err) {
                    Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, err);
                } catch (SQLException err) {
                    Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, err);
                }
            }

        } catch (IOException err) {
            Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, err);
        }

    }

    @Override
    public void run() {
        curThread = tempT;
        System.out.println("2. " + client.isReady());
        if (this.client.isReady()) {
            try {
                //re-initialized client's current class
                for (SuperClass sc : classrooms) {
                    if (sc.getClassId().equals(client.getCurClassroom())) {
                        this.curClass = sc;
                        break;
                    }
                }
                if (this.client.getType() == 1) //if is a lecturer
                {
                    this.setGo();
                } else {
                    System.out.println("User is ready!");
                    standBy();

                    if (this.client.getType() == 2) {
                        //Tell users to start the class
                        try {
                            System.out.println("Tell User!");
                            DataOutputStream dos = new DataOutputStream(client.getSocket().getOutputStream());
                            dos.writeBoolean(true);
                            System.out.println("Finished Telling");
                        } catch (IOException ex) {
                            Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (this.client.getType() == 1 && this.client.isCreatingClass()) {
            createClass();                          //Create classroom
        } else if (!(this.client.isCreatingClass())) {    //is a student 
            try {
                SuperClass s;
                DataOutputStream os = new DataOutputStream(client.getSocket().getOutputStream());
                s = checkForClass();
                if (s != null) {   //Class found
                    //Join Class
                    os.writeUTF("Found!");
                    curClass = s;
                    ObjectOutputStream oos = new ObjectOutputStream(client.getSocket().getOutputStream());
                    System.out.println(s.getQuesByIndex(0).getQuestion());
                    if (s != null) {
                        this.client.setCurClassroom(s.getClassId());
                        oos.writeObject(s);

                    }
                    //oos.close();
                    //os.writeInt(1);

                    //standBy();
                    /*if(this.client.getType() == 2)
                     {
                     //Tell users to start the class
                     try {
                     System.out.println("Tell User!");
                     DataOutputStream dos = new DataOutputStream(client.getSocket().getOutputStream());
                     dos.writeBoolean(true);
                     System.out.println("Finished Telling");
                     } catch (IOException ex) {
                     Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     }*/
                } else {      //Class not found
                    //os.writeInt(0);
                    os.writeUTF("NA");
                }
                //os.close();
                //standBy();
            } catch (IOException ex) {
                Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private void createClass() {
        SuperClass newClass;
        ObjectInputStream is;
        DataOutputStream os;
        try {
            is = new ObjectInputStream(client.getSocket().getInputStream());
            newClass = (SuperClass) is.readObject();

            //Check if class exist in database before, stop if already exist
            if (db.getClassroom(newClass.getClassId()) != null) {
                //Class code already exist
                os = new DataOutputStream(client.getSocket().getOutputStream());
                os.writeUTF("Class Code already exist. Please try other pin number\n");
                //os.close();
                //is.close();
                return;
            }

            //is.close();
            newClass.setCreator(Thread.currentThread());

            //insert to database
            //insert classroom
            String query = "INSERT INTO " + Database.tables[0] + " VALUES ('"
                    + newClass.getClassId() + "', '"
                    + this.client.getClientId() + "');";

            db.insertDB(query);
            System.out.println(query);
            System.out.println("Class done");
            //insert all questions
            for (QuizQuestion q : newClass.getAllQuestions()) {
                query = "INSERT INTO " + Database.tables[3]
                        + " (question, timing, classroomId, score) "
                        + "VALUES ('"
                        + q.getQuestion() + "', '"
                        + q.getTiming() + "', '"
                        + newClass.getClassId() + "', '"
                        + q.getScore() + "');";

                db.insertDB(query);
                System.out.println(query);
                System.out.println("Q" + q.getQuesId() + "done!");

                //insert all answer
                for (AnswerChoice ac : q.getAllChoices()) {

                    query = "INSERT INTO " + Database.tables[2]
                            + " (quizquesId, description, isCorrect) "
                            + "VALUES ("
                            + "(SELECT qqId FROM quizques WHERE question='" + q.getQuestion() + "')" + ", '"
                            + ac.getDescription() + "', '"
                            + (ac.getIsCorrect() ? 1 : 0) + "') ";
                    db.insertDB(query);
                    System.out.println(query);
                    System.out.println("Ans" + ac.getDescription() + "done!");
                }
            }

            System.out.println(query);
            db.insertDB(query);
            System.out.println("Inserted new class!");

        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void standBy() throws InterruptedException {
        //Stand by the thread to wait for starting the quiz session
        while (!(SuperClass.started)) {
            try {
                //if(!(this.getState().equals(Thread.State.WAITING)))
                //{
                //   System.out.println("\nWAITTTTT");
                if (!(client.isReady())) {
                    System.out.println("sadsda" + this.client.isReady());
                    DataInputStream dis = new DataInputStream(client.getSocket().getInputStream());
                    this.client.setIsReady(dis.readBoolean());
                    System.out.println("Server knows you are ready! :)");
                }
                //this.wait();
                //}

            } catch (IOException ex) {
                Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (this.client.getType() == 2) {
            //Tell users to start the class
            try {
                System.out.println("Tell User!");
                DataOutputStream dos = new DataOutputStream(client.getSocket().getOutputStream());
                dos.writeBoolean(true);
                System.out.println("Finished Telling");
            } catch (IOException ex) {
                Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private synchronized void setGo() throws InterruptedException {

        try {
            System.out.println("Sent yo");
            DataOutputStream dos = new DataOutputStream(client.getSocket().getOutputStream());
            /*for (Client client : clients ) {
             if(client.getCurClassroom().equals(curClass.getClassId())) {
             if(!(client.isReady()))
             {
             dos.writeBoolean(false);
             return;
             }
             }
             }   */
            System.out.println("Waked all!");
            dos.writeBoolean(true);
            SuperClass.started = true;
            this.notifyAll();    //Wake up all threads
            System.out.println("Waked all!");
        } catch (IOException ex) {
            Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private SuperClass checkForClass() {
        DataInputStream is;
        //ObjectOutputStream oos;
        try {
            is = new DataInputStream(client.getSocket().getInputStream());

            String classCode = is.readUTF();
            if (db.getClassroom(classCode) != null) {
                db.loadSuperClassData(classCode);
            }
            System.out.println(classCode);
            for (SuperClass c : classrooms) {
                if (c.getClassId().equals(classCode)) {
                    //Class is found!
                    this.client.setCurClassroom(classCode);
                    //MultiThreadedServer.db.printAllQuestion(c);
                    //is.close();
                    db.printAllQuestion(c);
                    return c;
                }
            }
            //is.close();

        } catch (IOException ex) {
            Logger.getLogger(MultiThreadedServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static Client findClientById(String c, int type) {
        for (Client client : clients) {
            if (client.getClientId().equals(c) && client.getType() == type) {
                return client;
            }
        }
        return null;
    }

}
