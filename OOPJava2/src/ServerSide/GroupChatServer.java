/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerSide;

import java.io.*;
import java.net.*;

/**
 *
 * @author ting
 */
public class GroupChatServer {
    
    // The server socket
    private static ServerSocket serverSocket = null;
    // The client socket.
    private static Socket clientSocket = null;

    // Enables the max 40 students + lecturer to access 
    private static final int maxClients = 40;
    private static final clientThread[] threads = new clientThread[maxClients];
    
    public static int portNumber = 2222;//set the port number to 2222

    public static void main(String args[]) { 
        if (args.length < 1) {
            System.out.println("Current Port No =" + portNumber);
        } else {
            portNumber = Integer.valueOf(args[0]).intValue();
        }

        //Open a server connection
        try {
            serverSocket = new ServerSocket(portNumber);            
        } catch (IOException e) {
            //portNumber = (int)(65536*Math.random());
            //portGenerator(portNumber);
        }

        // accept the clientsocket if exist
        while (true) {
            try {
                clientSocket = serverSocket.accept();
                
                int i=0;
                
                for ( i = 0; i < maxClients; i++) {
                    if (threads[i] == null) {
                        (threads[i] = new clientThread(clientSocket, threads)).
                            start();
                        break;
                    }
                }

                // to ensure that only fixed number of clients can be accepted
                if (i == maxClients) {
                    PrintStream output = new PrintStream(clientSocket.
                            getOutputStream());
                    output.println("Max clients are 40. Try again later.");
                    output.close();
                    clientSocket.close();
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
    
    public static void portGenerator (int portNumber){
        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            portNumber = (int)(65536*Math.random());
            portGenerator(portNumber);
        }
    }
}

// The chat client thread opens the input and the output streams for a client, 
// ask the client's name, informs all the clients connected to the server about 
// the fact that a new client has joined the chat room, and as long as it 
// receive data, send that data back to all other clients. When a client leaves 
// the chat room this thread informs also all the clients about that fact
class clientThread extends Thread {
    private DataInputStream input = null;
    private DataOutputStream output = null;
    private Socket clientSocket = null;
    private final clientThread[] threads;
    private int maxClients;

    public clientThread(Socket clientSocket, clientThread[] threads) {
        this.clientSocket = clientSocket;
        this.threads = threads;
        maxClients = threads.length;
    }

    public void run() {
        int maxClientsCount = this.maxClients;
        clientThread[] threads = this.threads;

        try {
            //Create input and output streams for this client.
            input = new DataInputStream(clientSocket.getInputStream());
            output = new DataOutputStream(clientSocket.getOutputStream());
            output.writeUTF("Enter your name.");
            String Username = input.readUTF().trim();
            output.writeUTF("Hello " + Username+ "." +"Welcome to discussion" 
                    + " room.");

            for (int i = 0; i < maxClientsCount; i++) {
                if (threads[i] != null && threads[i] != this) {
                    threads[i].output.writeUTF("Announcment: A new user " 
                        + Username + " entered the chat room.");
                }
            }

            while (true) {
                String message = input.readUTF();
                if (message.startsWith("/quit")) {
                break;
                }
                for (int i = 0; i < maxClientsCount; i++) {
                    if (threads[i] != null) {
                        threads[i].output.writeUTF("[" + Username + "]; " 
                                + message);
                    }
                }
            }

            for (int i = 0; i < maxClientsCount; i++) {
                if (threads[i] != null && threads[i] != this) {
                    threads[i].output.writeUTF("*** The user " + Username
                        + " is leaving the chat room !!! ***");
                }
            }

            output.writeUTF("*** Bye " + Username + " ***");

            //Set the current thread variable to null so that a new client
            //could be accepted by the server.
            for (int i = 0; i < maxClients; i++) {
                if (threads[i] == this) {
                    threads[i] = null;
                }
            }

            //Close the output stream, close the input stream, close the socket.
            input.close();
            output.close();
            clientSocket.close();
        } catch (IOException e) {
            
        }
    }
}
