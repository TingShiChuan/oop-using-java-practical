/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_assignment2;

import java.net.*;
import java.io.*;
import java.util.Scanner;

/**
 *
 * @author janicechau
 */
public class ClientChat implements Runnable{

    private static Socket clientSocket = null;
    // The output stream
    private static DataOutputStream os = null;
    // The input stream
    private static DataInputStream is = null;
    private static Scanner inputLine = null;
    private static boolean closed = false;

    public static void main(String[] args) {
        // to generate random port number that available
        int portNumber = 2222;
        // The default host.
        String host = "localhost";

        if (args.length < 2) {
            System.out.println("Usage: java MultiThreadChatClient "
                    + "<host> <portNumber>\n" + "Now using host=" + host
                    + ", portNumber=" + portNumber);
        } else {
            host = args[0];
            portNumber = Integer.valueOf(args[1]).intValue();
        }

        try {//Open a socket on a given host and port. 
            clientSocket = new Socket(host, portNumber);
            inputLine = new Scanner(System.in);

            //Open input and output streams.
            os = new DataOutputStream(clientSocket.getOutputStream());
            is = new DataInputStream(clientSocket.getInputStream());
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + host);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to the host"
                    + host);
        }

        //If everything has been initialized then write some data to the
        //socket with an opened connection to on the portNumber.
        if (clientSocket != null && os != null && is != null) {
            try {// Create a thread to read from the server. 
                new Thread(new ClientChat()).start();
                while (!closed) {
                    System.out.println((is.readUTF().trim()));
                }
                //Close the output stream, input stream and the socket.
                os.close();
                is.close();
                clientSocket.close();
            } catch (IOException e) {
                System.err.println("IOException:  " + e);
            }
        }
    }

    //the run method for the thread
    public void run() {
        //Keep on reading from the socket until receive "Bye" from the server.  
        //Once received, break.
        String responseLine;

        try {
            while ((responseLine = is.readUTF()) != null) {
                System.out.println(responseLine);
                if (responseLine.indexOf("*** Bye") != -1) {
                    break;
                }
            }
            closed = true;
        } catch (IOException e) {
            System.err.println("IOException:  " + e);
        }
    }
}
