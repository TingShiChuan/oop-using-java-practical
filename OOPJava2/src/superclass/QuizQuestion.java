/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superclass;

import java.io.Serializable;
import java.net.*;
import java.util.*;

/**
 *
 * @author ting
 */
public class QuizQuestion implements Serializable {
    
    private int score;
    
    private String question;
    
    private int timing;
    
    private String quesId;
    
    private AnswerChoice [] choices = new AnswerChoice[4];
    
    private List<Client> rankforAnswer = new ArrayList<>();
            
    public QuizQuestion() {
        
    }
    
    public QuizQuestion(String ques, int score, int timing) {
        this.question = ques;
        this.score = score;
        this.timing = timing;
    }
    
    public QuizQuestion(String quesId, String ques, int score, int timing) {
        this.quesId = quesId;
        this.question = ques;
        this.score = score;
        this.timing = timing;
    }
    
    public void setChoices(int no, String desc, boolean isCorrectAnswer) {
        if(no >= 0 && no <= 3)
            choices[no] = new AnswerChoice(desc, isCorrectAnswer); 
    }
    
    public AnswerChoice [] getAllChoices() {
        return this.choices;
    }
    
    public AnswerChoice getChoiceByIndex(int i) {
        return this.choices[i];
    }
    
    public void setQuestionId(String quesId) {
        this.quesId = quesId;
    }
    
    public String getQuesId() {
        return this.quesId;
    }
    
    public void setQuestionString(String ques) {
        this.question = ques;
    }
    
    public String getQuestion() {
        return this.question;
    }
        
    public void setScore(int score) {
        this.score = score;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public void setTiming(int timing) {
        this.timing = timing;
    }
    
    public int getTiming() {
        return this.timing;
    }
    
    public void addRankClient(Client c) {
        rankforAnswer.add(c);
    }
    
    public void removeRankClient(Client c) {
        rankforAnswer.remove(c);
    }
    
    public List<Client> getRanking() {
        return this.rankforAnswer;
    }
           
}
