/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superclass;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.jdbc.ScriptRunner;

/**
 *
 * @author ting
 */
public class Database {
    
    private String dbUrl;
    private String usrName;
    private String pW;
    private String schemaName;
    private static String driverUrl = "com.mysql.jdbc.Driver";
    private static String [] scripts = {
        "SuperClass_classroom.sql",  
        "SuperClass_Client.sql",
        "SuperClass_quizanswer.sql",
        "SuperClass_quizques.sql",
        "SuperClass_student.sql"
    };
    
    private static String [] tables = {
        "classroom",  
        "Client",
        "quizanswer",
        "quizques",
        "student"
    };
    
    
    
    public Database(String dbUrl, String schemaName, String usrName, String pW) {
        this.dbUrl = dbUrl;
        this.schemaName = schemaName;
        this.usrName = usrName;
        this.pW = pW;
    }
    
    public String getSchema() {
        return this.schemaName;
    }
    
    public String getDbUrl() {
        return this.dbUrl;
    }
    
    public void initDB() throws SQLException, ClassNotFoundException {
        
        //Load driver
        Class.forName(driverUrl);
        
        
        //Create database connection
        //Connection connection = DriverManager.getConnection(dbUrl, usrName, pW);
        //DatabaseMetaData metadata = connection.getMetaData();
        
        
        for(String script : scripts) {
            try {
                new ScriptRunner(
                        DriverManager.getConnection(dbUrl, usrName, pW))
                        .runScript(new BufferedReader(new FileReader("mysqlscripts/" + script)));
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        
        /*
        //Variable declarations
        ResultSet resultSet;
        Statement statement = connection.createStatement();
        
        //Check is schema created
        resultSet = metadata.getSchemas(schemaName, null);
        if(!(resultSet.next())) {
            
            //create schema
            int result = statement.executeUpdate(
                "CREATE SCHEMA " + schemaName);
            System.out.println(result);
            System.out.println("Schema is created!");
        }
        else {
            System.out.println("Schema exist!");
        }
        
        //Check is table created
        resultSet = metadata.getTables(null, null, "BOOKS", null);
        if(!(resultSet.next())) {
            //create tables
            int result = statement.executeUpdate(
                "CREATE TABLE '" + schemaName + "'.`Client` (\n" +
                    "  `cId` VARCHAR(200) NOT NULL COMMENT '',\n" +
                    "  `cName` VARCHAR(45) NULL COMMENT '',\n" +
                    "  `cType` INT NULL COMMENT '',\n" +
                    "  `lastlogin` DATETIME NULL COMMENT '',\n" +
                    "  `createdDate` DATETIME NULL COMMENT '',\n" +
                    "  PRIMARY KEY (`cId`)  COMMENT '');");
            
            System.out.println(result);
            System.out.println("Table is created!");
        }
        else {
            System.out.println("Table is created!");
        }
        
        int result = statement.executeUpdate(
                "INSERT INTO BOOKS VALUES "
                        + "(1001, 'Java for dummies', 'Tan Ah Teck', 11.11, 11),"
                        + "(1002, 'More Java for dummies', 'Tan Ah Teck', 22.22, 22),"
                        + "(1003, 'More Java for more dummies', 'Mohammad Ali', 33.33, 33),"
                        + "(1004, 'A Cup of Java', 'Kumar', 44.44, 44),"
                        + "(1005, 'A Teaspoon of Java', 'Kevin Jones', 55.55, 55)");
        
        
        if(result == 0) {
            System.out.println("Insert value successful!");
        }
        
        resultSet = statement.executeQuery(
                    "SELECT * FROM BOOKS");
        
        while(resultSet.next()) {
            System.out.println(
                resultSet.getString(1) + ", " +
                resultSet.getString(2) + ", " +
                resultSet.getString(3) + " " 
            );
        }*/
    }
    
    public void insertDB(String query) {
        Connection connection;
        try {
            //Load Driver
            Class.forName(driverUrl);
            //Create db connection
            connection = DriverManager.getConnection(dbUrl, usrName, pW);
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate(query);
            if (result == 0) {
                System.out.println("Record inserted!");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ResultSet retriveDB(String query) {
        Connection connection;
        try {
            //Load Driver
            Class.forName(driverUrl);
            //Create db connection
            connection = DriverManager.getConnection(dbUrl, usrName, pW);
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
