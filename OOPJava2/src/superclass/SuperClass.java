/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superclass;


import java.io.*;
import java.util.*;

/**
 *
 * @author ting
 */
public class SuperClass implements Serializable {

    private String classId;
    
    public static boolean started;
    
    private List<Client> clients = new ArrayList<>();
    
    private List<QuizQuestion> questions = new ArrayList<>();
    
    //The thread (lecturer as client) who created the class
    private Thread createdBy;   
    
    
    public SuperClass() {
        
    }
    
    public SuperClass(String classId) {
        this.classId = classId;
    }
    
    public void addQuestion(QuizQuestion q) {
        questions.add(q);
    }
    
    public List<QuizQuestion> getAllQuestions() {
        return this.questions;
    }
    
    public void setAllClients(List<Client> allClients) {
        this.clients = allClients;
    }
    
    public List<Client> getAllClients() {
        return this.clients;
    }
    
    public QuizQuestion getQuesByIndex(int i) {
        return this.questions.get(i);
    }
    
    public int getQuesIndexById(String id) {
        for(QuizQuestion q : questions) {
            if(q.getQuesId().equals(id)) {
                return questions.indexOf(q);
            }
        }
        return -1;
    }
    
    public void setClassId(String Id) {
        this.classId = Id;
    }
    
    public String getClassId() {
        return this.classId;
    }
    
    public void setCreator(Thread creator) {
        this.createdBy = creator;
    }
    
    public Thread getCreator() {
        return this.createdBy;
    }
    
   public void printAllQuestion(SuperClass demo) {
        List<QuizQuestion> questions = demo.getAllQuestions();
        int i = 1 ;
        for(QuizQuestion q : questions) {
            System.out.print(i);
            System.out.print("\t" + q.getQuestion());
            System.out.print("\t" + q.getScore());
            System.out.print("\t" + q.getTiming());
            System.out.print("\n");
            for(int j = 0; j < q.getAllChoices().length; j++) {
                if(q.getChoiceByIndex(j) != null) {
                    System.out.print(j);
                    System.out.print("\t" + q.getChoiceByIndex(j).getDescription());
                    System.out.print("\t" + q.getChoiceByIndex(j).getIsCorrect());
                    System.out.print("\n");
                }
            }
            System.out.print("\n\n");
        }
    } 
    
}
