/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superclass;

import java.io.Serializable;

/**
 *
 * @author ting
 */
public class AnswerChoice implements Serializable {
    
    private String choiceDescription;
    
    private int totalSelected;
    
    private boolean isCorrectAnswer;
    
    public AnswerChoice(String desc, boolean isCorrectAnswer) {
        this.choiceDescription = desc;
        this.isCorrectAnswer = isCorrectAnswer;
    }
    
    public void setDescription(String desc) {
        this.choiceDescription = desc;
    }
    
    public String getDescription() {
        return this.choiceDescription;
    }
    
    public void addSelected() {
        this.totalSelected++;
    }
    
    public void resetSelected() {
        this.totalSelected = 0;
    }
    
    public int getTotalSelected() {
        return this.totalSelected;
    }

    public void setisCorrect(boolean value) {
        this.isCorrectAnswer = value;
    }
    
    public boolean getIsCorrect() {
        return this.isCorrectAnswer;
    }
    
    
}
