/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superclass;

import java.io.*;
import java.net.*;


/**
 *
 * @author ting
 */
public class Client implements Serializable {
    
    
    private Socket clientSoc;
    
    private boolean isCreatingClass;
    
    private String clientId;
    
    private String clientName;
    
    private int clientType;
    
    private int score;
    
    private String curClassroom;
    
    private boolean isReady;
    
    public void setIsReady(boolean state) {
        this.isReady = state;
    }
    
    public boolean isReady() {
        return this.isReady;
    }
    
    public void setIsCreatingClass(boolean state) {
        this.isCreatingClass = state;
    }
    
    public boolean isCreatingClass() {
        return this.isCreatingClass;
    }
    
    public void setType(int type) {
        this.clientType = type;
    }
    
    public Client(Socket clientSoc) {
        this.clientSoc = clientSoc;
    }
    
    public Client(String name, int clientType) {
        this.clientName = name;
        this.clientType = clientType;
    }
    
    public Client(Socket clientSoc, String name, int clientType) {
        this.clientSoc = clientSoc;
        this.clientName = name;
        this.clientType = clientType;
    }
    
    public void setSocket(Socket clientSoc) {
        this.clientSoc = clientSoc;
    }
   
    public void setClientId(String cId) {
        this.clientId = cId;
    }
    
    public String getClientId() {
        return this.clientId;
    }
    
    public void setCurClassroom(String cId) {
        this.curClassroom = cId;
    }
    
    public String getCurClassroom() {
        return this.curClassroom;
    }
    
    public Socket getSocket() {
        return this.clientSoc;
    }
    
    public int getType() {
        return this.clientType;
    }
    
    public void setName(String name) {
        this.clientName = name;
    }
    
    public String getName() {
        return this.clientName;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public void addScore(int score) {
        this.score += score;
    }
    
    public String toString(int score) {
        return score + " ";
    }
}
