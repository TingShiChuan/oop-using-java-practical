use SuperClass;

SELECT  qa.*, qq.*
FROM classroom cr
JOIN quizques qq
ON qq.classroomId = cr.crId
JOIN quizanswer qa
ON qa.quizquesId = qq.qqId
WHERE cr.crId = 'C001';