CREATE DATABASE  IF NOT EXISTS `SuperClass` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `SuperClass`;
-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 127.0.0.1    Database: SuperClass
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `quizanswer`
--

DROP TABLE IF EXISTS `quizanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizanswer` (
  `quizansId` int(11) NOT NULL AUTO_INCREMENT,
  `quizquesId` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `isCorrect` int(11) DEFAULT NULL,
  `totalSelected` int(11) DEFAULT NULL,
  PRIMARY KEY (`quizansId`),
  KEY `fk_ans_ques_idx` (`quizquesId`),
  CONSTRAINT `fk_ans_ques` FOREIGN KEY (`quizquesId`) REFERENCES `quizques` (`qqId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizanswer`
--

LOCK TABLES `quizanswer` WRITE;
/*!40000 ALTER TABLE `quizanswer` DISABLE KEYS */;
INSERT INTO `quizanswer` VALUES (1,1,'A Human Language',0,NULL),(2,1,'A Computer',0,NULL),(3,1,'A Programming Language',1,NULL),(4,1,'Coffee',1,NULL),(5,2,'Our Lecturer',1,NULL),(6,2,'Not a Human',0,NULL),(7,2,'Crocodile',0,NULL),(8,2,'Best Friend',0,NULL);
/*!40000 ALTER TABLE `quizanswer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-25 17:28:57
