/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac1;

/**
 *
 * @author ting
 */
public class TestScores {
    
    private double [] t = new double[5];
    
    TestScores() {
        
    }
    
    TestScores(double [] t) {
        for(int i = 0; i < t.length; i++) {
            if(t[i] < 0 || t[i] > 100) {
                throw new IllegalArgumentException("Scores must between 0 to 100");
            }
        }
        this.t = t;
    }

   
    public void printScore() {
        for(int i = 0; i < t.length; i++) {
            System.out.print(t[i] + "\n");
        }
        
    }
    
    
}
