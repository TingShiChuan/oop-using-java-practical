/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopprac1;

import java.util.Scanner;

/**
 *
 * @author ting
 */
public class OOPPrac1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{

            double [] newScores = new double[5];
            Scanner input = new Scanner(System.in);

            for(int i = 0; i < 5; i++) {
                System.out.printf("Enter the %d score: ", i);
                newScores[i] = input.nextDouble();
            }

            TestScores t = new TestScores(newScores);
            t.printScore();
            
        }
        catch(IllegalArgumentException err) {
            System.out.println(err);
        }
        catch(Exception err) {
            System.out.println(err);
        }
        finally { //finally clause will always be executed whether there is an exception or not
            System.out.println("END");
        }
    }
    
}
