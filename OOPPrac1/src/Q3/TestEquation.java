/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q3;

import java.util.Scanner;

/**
 *
 * @author ting
 */
public class TestEquation {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char cont = 'Y';
        while (cont == 'Y') {
            try {
                int a, b, c;

                System.out.print("Enter the first number: ");
                a = input.nextInt();
                System.out.print("Enter the second number: ");
                b = input.nextInt();
                System.out.print("Enter the third number: ");
                c = input.nextInt();

                double result = root(a, b, c);

                System.out.println("Result is " + result);

            } catch (IllegalArgumentException err) {
                System.out.println(err);
                
            } catch (Exception err) {
                System.out.println(err);
            }
            finally {
                System.out.println("Y to continue, N to quit.");
                cont = input.next().charAt(0);
            }
            
        }

    }

    static double root(double A, double B, double C)
            throws IllegalArgumentException {
        // Returns the larger of the two roots of
        // the quadratic equation A*x*x + B*x + C = 0.
        // (Throws an exception if A == 0 or B*B-4*A*C < 0.)
        if (A == 0) {
            throw new IllegalArgumentException("A can't be zero.");
        } else {
            double disc = B * B - 4 * A * C;
            if (disc < 0) {
                throw new IllegalArgumentException("Discriminant < zero.");
            }
            return (-B + Math.sqrt(disc)) / (2 * A);
        }
    }

}
