/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical3;

/**
 *
 * @author ting
 */
public class Tuple<K, V> {
    
    K data1;
    V data2;
    
    Tuple() {
        
    }
    
    Tuple(K data1, V data2) {
        this.data1 = data1;
        this.data2 = data2;
    }
    
    @Override
    public String toString() {
        String value = "First value is " + this.data1 + ", Second value is " + this.data2;
        return value;
    }
}
