/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical3;

/**
 *
 * @author ting
 */
public class Q2 {
    public static void main(String [] args) {
        Tuple <String, Integer> tuple1 = new Tuple("ONE", 1);
        Tuple <String, Double> tuple2 = new Tuple("TWO" , 2.2);
        Tuple < Integer, Integer> tuple3 = new Tuple(3 , 3);
        Tuple< String, String> tuple4 = new Tuple("FOUR" , "FOUR");
        
        System.out.println(tuple1.toString());
        System.out.println(tuple2.toString());
        System.out.println(tuple3.toString());
        System.out.println(tuple4.toString());
    }
    
}
