/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical3;

/**
 *
 * @author ting
 */
public class Q1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        GenericClass [] data = new GenericClass[2];
        
        GenericClass<String> stringObj = new GenericClass<>("Ting");
        GenericClass<Integer> intObj = new GenericClass<>(10);
        
        data[0] = stringObj;
        data[1] = intObj;
        
        GenericClass.exchange(data, 0, 1);
        
    }
    
}
