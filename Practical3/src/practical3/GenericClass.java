/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical3;

/**
 *
 * @author ting
 */
public class GenericClass<E> {
    
    E data;
    
    GenericClass() {
        
    }
    
    GenericClass(E data1) {
        this.data = data1;
    }
    
    public static void exchange(GenericClass [] array, int index1, int index2) {
        
        System.out.println("Before swap...");
        for(int i = 0; i < 2; i++) {
            System.out.println("Element 1 is " + array[index1].data);
            System.out.println("Element 2 is " + array[index2].data);
            
            //swap
            GenericClass e = array[index2];
            array[index2] = array[index1];
            array[index1] = e;
            
            System.out.println("After swap...");
        }
        
        
    }
    
    
    
    
}
